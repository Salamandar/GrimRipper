project('cdparanoia',
    'c',
    version: '10.2',
    meson_version: '>=0.0.0',
    default_options: [
        'c_std=gnu99',
    ],
)

pkgconfig = import('pkgconfig')

cc = meson.get_compiler('c')

math = cc.find_library('m', required : false)

ld_version = '0.' + meson.project_version()

libcdda_interface = library('cdda_interface',
    'interface/common_interface.c',
    'interface/cooked_interface.c',
    'interface/interface.c',
    'interface/scan_devices.c',
    'interface/scsi_interface.c',
    'interface/smallft.c',
    'interface/test_interface.c',
    'interface/toc.c',
    dependencies: [
        math,
    ],
    version: ld_version,
    install: true,
)

libcdda_paranoia = library('cdda_paranoia',
    'paranoia/gap.c',
    'paranoia/isort.c',
    'paranoia/overlap.c',
    'paranoia/paranoia.c',
    'paranoia/p_block.c',
    link_with: [
        libcdda_interface,
    ],
    dependencies: [
        math,
    ],
    version: ld_version,
    install: true,
)

paranoia = executable('paranoia',
    'main.c',
    'report.c',
    'header.c',
    'buffering_write.c',
    'cachetest.c',
    link_with: [
        libcdda_paranoia,
        libcdda_interface,
    ],
    dependencies: [
        math,
    ],
    install: true,
)

install_headers(
    'interface/cdda_interface.h',
    'paranoia/cdda_paranoia.h',
    'utils.h',
)

install_man('cdparanoia.1')
