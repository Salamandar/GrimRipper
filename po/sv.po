# Swedish translation for GrimRipper.
# This file is distributed under the same license as the GrimRipper package.
# Copyright (C) 2007, 2009 Free Software Foundation, Inc.
# Daniel Nylander <po@danielnylander.se>, 2007, 2009.
# Andreas Rönnquist <andreas@ronnquist.net> 2012-2018
#
msgid ""
msgstr ""
"Project-Id-Version: grimripper\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-03-14 13:22+0100\n"
"PO-Revision-Date: 2018-04-07 14:29+0200\n"
"Last-Translator: Andreas Rönnquist <andreas@ronnquist.net>\n"
"Language-Team: Swedish <tp-sv@listor.tp-sv.se>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural= n!=1;\n"
"X-Generator: Poedit 1.8.11\n"

#: src/main.c:131 src/interface.c:230
msgid "Rip"
msgstr "Extrahera"

#: src/main.c:141
msgid "Track"
msgstr "Spår"

#: src/main.c:149
msgid "Artist"
msgstr "Artist"

#: src/main.c:157 src/main.c:165
msgid "Title"
msgstr "Titel"

#: src/main.c:173
msgid "Time"
msgstr "Tid"

#: src/main.c:199
msgid ""
"'cdparanoia' was not found in your path. GrimRipper requires cdparanoia to rip "
"CDs."
msgstr ""
"\"cdparanoia\" hittades inte i dina sökvägar. GrimRipper kräver cdparanoia för "
"att extrahera cd-skivor."

#: src/main.c:244 src/main.c:269 src/main.c:869
msgid "<b>Checking disc...</b>"
msgstr "<b>Kontrollerar skiva...</b>"

#: src/main.c:538
msgid "<b>Getting disc info from the internet...</b>"
msgstr "<b>Hämtar skivinformation från Internet...</b>"

#: src/main.c:599
msgid "<b>Reading disc...</b>"
msgstr "<b>Läser skiva...</b>"

#: src/callbacks.c:61 src/callbacks.c:319 src/callbacks.c:351
#: src/callbacks.c:362 src/callbacks.c:373 src/callbacks.c:385
#: src/interface.c:606 src/interface.c:685 src/interface.c:784
#: src/interface.c:913
#, c-format
msgid "%dKbps"
msgstr "%dKbps"

#: src/callbacks.c:750
msgid "No CD is inserted. Please insert a CD into the CD-ROM drive."
msgstr "Ingen skiva är inmatad. Mata in en skiva i cd-rom-enheten."

#: src/callbacks.c:771 src/callbacks.c:799 src/callbacks.c:826
#: src/callbacks.c:853 src/callbacks.c:880 src/callbacks.c:908
#: src/callbacks.c:935 src/callbacks.c:962
#, c-format
msgid ""
"%s was not found in your path. GrimRipper requires it to create %s files. All %s "
"functionality is disabled."
msgstr ""
"%s hittades inte i dina sökvägar. GrimRipper kräver det för att skapa %s-filer. "
"All %s-funktionalitet har inaktiverats."

#: src/callbacks.c:1060
msgid "Select all for ripping"
msgstr "Markera alla för extrahering"

#: src/callbacks.c:1066
msgid "Deselect all for ripping"
msgstr "Avmarkera alla för extrahering"

#: src/callbacks.c:1072
msgid "Capitalize Artists & Titles"
msgstr "Använd stor bokstav i Artist & Titel"

#: src/callbacks.c:1102
msgid "Swap Artist <=> Title"
msgstr "Växla Artist <=> Titel"

#: src/interface.c:85
msgid "CDDB Lookup"
msgstr "CDDB-uppslag"

#: src/interface.c:91 src/interface.c:372
msgid "Preferences"
msgstr "Inställningar"

#: src/interface.c:102
msgid "About"
msgstr ""

#: src/interface.c:135
msgid "Disc:"
msgstr "Skiva:"

#: src/interface.c:139
msgid "Album Artist:"
msgstr "Albumartist:"

#: src/interface.c:144
msgid "Album Title:"
msgstr "Albumtitel:"

#: src/interface.c:149
msgid "Single Artist"
msgstr "En Artist"

#: src/interface.c:157
msgid "First track number:"
msgstr "Första spårnummer:"

#: src/interface.c:170
msgid "Track number width in filename:"
msgstr "Bredd på spårnummer i filnamn:"

#: src/interface.c:197
msgid "Genre / Year:"
msgstr "Genre / År:"

#: src/interface.c:389 src/interface.c:395
msgid "Destination folder"
msgstr "Målmapp"

#: src/interface.c:399
msgid "Create M3U playlist"
msgstr "Skapa M3U-spellista"

#: src/interface.c:407
msgid "CD-ROM device: "
msgstr "Cd-rom-enhet: "

#: src/interface.c:416
msgid ""
"Default: /dev/cdrom\n"
"Other example: /dev/hdc\n"
"Other example: /dev/sr0"
msgstr ""
"Standard: /dev/cdrom\n"
"Annat exempel: /dev/hdc\n"
"Annat exempel: /dev/sr0"

#: src/interface.c:420
msgid "Eject disc when finished"
msgstr "Mata ut skivan när den är färdig"

#: src/interface.c:428
msgid "General"
msgstr "Allmänt"

#: src/interface.c:448
msgid ""
"%A - Artist\n"
"%L - Album\n"
"%N - Track number (2-digit)\n"
"%Y - Year (4-digit or \"0\")\n"
"%T - Song title"
msgstr ""
"%A - Artist\n"
"%L - Album\n"
"%N - Spårnummer (2-siffrigt)\n"
"%Y - Årtal (4-siffrigt eller \"0\")\n"
"%T - Låttitel"

#: src/interface.c:453
#, c-format
msgid "%G - Genre"
msgstr "%G - Genre"

#: src/interface.c:469
msgid "Album directory: "
msgstr "Albumkatalog: "

#: src/interface.c:474 src/prefs.c:776
msgid "Playlist file: "
msgstr "Spellistfil: "

#: src/interface.c:479 src/prefs.c:788 src/prefs.c:798
msgid "Music file: "
msgstr "Musikfil: "

#: src/interface.c:490
msgid ""
"This is relative to the destination folder (from the General tab).\n"
"Can be blank.\n"
"Default: %A - %L\n"
"Other example: %A/%L"
msgstr ""
"Det här är relativt till målmappen (från fliken Allmänt).\n"
"Kan vara blank.\n"
"Standard: %A - %L\n"
"Annat exempel: %A/%L"

#: src/interface.c:500
msgid ""
"This will be stored in the album directory.\n"
"Can be blank.\n"
"Default: %A - %L"
msgstr ""
"Det här kommer att lagras i albumkatalogen.\n"
"Kan vara blank.\n"
"Standard: %A - %L"

#: src/interface.c:509
msgid ""
"This will be stored in the album directory.\n"
"Cannot be blank.\n"
"Default: %A - %T\n"
"Other example: %N - %T"
msgstr ""
"Det här kommer att lagras i albumkatalogen.\n"
"Får inte vara blank.\n"
"Standard: %A - %T\n"
"Annat exempel: %N - %T"

#: src/interface.c:514
msgid "Filename formats"
msgstr "Filnamnsformat"

#: src/interface.c:519
msgid "Allow changing first track number"
msgstr "Tillåt att ändra första spårnummer"

#: src/interface.c:524
msgid "Filenames"
msgstr "Filnamn"

#: src/interface.c:558
msgid "WAV (uncompressed)"
msgstr "WAV (okomprimerad)"

#: src/interface.c:576
msgid "Variable bit rate (VBR)"
msgstr "Variabel bitfrekvens (VBR)"

#: src/interface.c:583
msgid "Better quality for the same size."
msgstr "Bättre kvalitet med samma storlek."

#: src/interface.c:589 src/interface.c:668 src/interface.c:769
#: src/interface.c:851 src/interface.c:896
msgid "Bitrate"
msgstr "Bitfrekvens"

#: src/interface.c:603 src/interface.c:682
msgid ""
"Higher bitrate is better quality but also bigger file. Most people use "
"192Kbps."
msgstr ""
"Högre bitfrekvens ger bättre kvalitet men även en större fil. De flesta "
"använder 192Kbps."

#: src/interface.c:612
msgid "MP3 (lossy compression)"
msgstr "MP3 (förstörande komprimering)"

#: src/interface.c:632
msgid "Quality"
msgstr "Kvalitet"

#: src/interface.c:642
msgid "Higher quality means bigger file. Default is 6."
msgstr "Högre kvalitet betyder större fil. Standard är 6."

#: src/interface.c:644
msgid "OGG Vorbis (lossy compression)"
msgstr "OGG Vorbis (förstörande komprimering)"

#: src/interface.c:691
msgid "AAC (lossy compression)"
msgstr "AAC (förstörande komprimering)"

#: src/interface.c:711 src/interface.c:817 src/interface.c:949
msgid "Compression level"
msgstr "Komprimeringsnivå"

#: src/interface.c:721 src/interface.c:961
msgid "This does not affect the quality. Higher number means smaller file."
msgstr "Det här påverkar inte kvaliteten. Högre tal betyder mindre fil."

#: src/interface.c:723
msgid "FLAC (lossless compression)"
msgstr "FLAC (förlustfri komprimering)"

#: src/interface.c:741
msgid "More formats"
msgstr "Fler format"

#: src/interface.c:782
msgid ""
"Higher bitrate is better quality but also bigger file. Most people use "
"160Kbps."
msgstr ""
"Högre bitfrekvens ger bättre kvalitet men även en större fil. De flesta "
"använder 160Kbps."

#: src/interface.c:790
msgid "OPUS (lossy compression)"
msgstr "OPUS (förstörande komprimering)"

#: src/interface.c:830
msgid ""
"This does not affect the quality. Higher number means smaller file. Default "
"is 1 (recommended)."
msgstr ""
"Det här påverkar inte kvaliteten. Högre tal betyder mindre fil. Standard är "
"1 (rekommenderat)."

#: src/interface.c:836
msgid "Hybrid compression"
msgstr "Hybrid-komprimering"

#: src/interface.c:845
msgid ""
"The format is lossy but a correction file is created for restoring the "
"lossless original."
msgstr ""
"Formatet är förstörande men en korrigeringsfil skapas för att återskapa det "
"förlustfria originalet."

#: src/interface.c:911
msgid "Higher bitrate is better quality but also bigger file."
msgstr "Högre bitfrekvens ger bättre kvalitet men även en större fil."

#: src/interface.c:919
msgid "Musepack (lossy compression)"
msgstr "Musepack (förstörande komprimering)"

#: src/interface.c:963
msgid "Monkey's Audio (lossless compression)"
msgstr "Monkey's Audio (förlustfri komprimering)"

#: src/interface.c:981
msgid "Encode"
msgstr "Kodning"

#: src/interface.c:1015
msgid "Get disc info from the internet automatically"
msgstr "Hämta skivinformation automatiskt från Internet"

#: src/interface.c:1023 src/interface.c:1067
msgid "Server: "
msgstr "Server: "

#: src/interface.c:1032
msgid "The CDDB server to get disc info from (default is gnudb.gnudb.org)"
msgstr ""
"CDDB-servern att hämta skivinformation från (standard är gnudb.gnudb.org)"

#: src/interface.c:1038 src/interface.c:1080
msgid "Port: "
msgstr "Port: "

#: src/interface.c:1047
msgid "The CDDB server port (default is 8880)"
msgstr "Port för CDDB-server (standard är 8880)"

#: src/interface.c:1054
msgid "Use an HTTP proxy to connect to the internet"
msgstr "Använd en HTTP-proxy för att ansluta till Internet"

#: src/interface.c:1102
msgid "Artist/Title separator: "
msgstr ""

#: src/interface.c:1113
msgid "Log to /var/log/grimripper.log"
msgstr "Logga till /var/log/grimripper.log"

#: src/interface.c:1118
msgid "Faster ripping (no error correction)"
msgstr "Snabbare extrahering (ingen felkorrigering)"

#: src/interface.c:1127
msgid "Advanced"
msgstr "Avancerat"

#: src/interface.c:1183 src/interface.c:1219
msgid "Ripping"
msgstr "Extrahering"

#: src/interface.c:1213
msgid "Total progress"
msgstr "Totalt förlopp"

#: src/interface.c:1225
msgid "Encoding"
msgstr "Kodning"

#: src/interface.c:1865
#, c-format
msgid "%d file created successfully"
msgid_plural "%d files created successfully"
msgstr[0] "%d fil skapades"
msgstr[1] "%d filer skapades"

#: src/interface.c:1874
#, c-format
msgid "There was an error creating %d file"
msgid_plural "There was an error creating %d files"
msgstr[0] "Det uppstod ett fel vid skapandet av %d fil"
msgstr[1] "Det uppstod ett fel vid skapandet av %d filer"

#: src/prefs.c:775 src/prefs.c:787
#, c-format
msgid "Invalid characters in the '%s' field"
msgstr "Ogiltiga tecken i fältet \"%s\""

#: src/prefs.c:797
#, c-format
msgid "'%s' cannot be empty"
msgstr "\"%s\" får inte vara tom"

#: src/prefs.c:812
msgid "Invalid proxy port number"
msgstr "Ogiltigt portnummer för proxy"

#: src/prefs.c:825
msgid "Invalid cddb server port number"
msgstr "Ogiltigt portnummer för cddb-server"

#: src/support.c:47
msgid "Overwrite?"
msgstr "Skriv över?"

#: src/support.c:60
#, c-format
msgid "The file '%s' already exists. Do you want to overwrite it?\n"
msgstr "Filen \"%s\" finns redan. Vill du skriva över den?\n"

#: src/support.c:66
msgid "Remember the answer for _all the files made from this CD"
msgstr "Kom ihåg svaret för _alla filer skapade från denna skiva"

#: src/threads.c:187
msgid ""
"No ripping/encoding method selected. Please enable one from the "
"'Preferences' menu."
msgstr ""
"Ingen metod för extrahering/kodning har valts. Aktivera en från menyn "
"\"Inställningar\"."

#: src/threads.c:219
msgid ""
"No tracks were selected for ripping/encoding. Please select at least one "
"track."
msgstr "Inga spår har markerats för extrahering/kodning. Välj minst ett spår."

#: src/threads.c:1299 src/threads.c:1301 src/threads.c:1305
msgid "Waiting..."
msgstr "Väntar..."

#~ msgid ""
#~ "An application to save tracks from an Audio CD \n"
#~ "as WAV, MP3, OGG, FLAC, Wavpack, Opus, Musepack, Monkey's Audio, and/or "
#~ "AAC files."
#~ msgstr ""
#~ "Ett program för att spara spår från en ljud-CD \n"
#~ "som WAV, MP3, OGG, FLAC, Wavpack, Opus, Musepack, Monkey's Audio och/"
#~ "eller AAC-filer."

#~ msgid "Split 'Artist/Title' in Title field"
#~ msgstr "Dela 'Artist/Titel' i titelfältet"

#~ msgid "Proprietary encoders"
#~ msgstr "Proprietära kodare"

#~ msgid "Higher quality means bigger file. Default is 60."
#~ msgstr "Högre kvalitet betyder större fil. Standard är 60."

#~ msgid "AAC (lossy compression, Nero encoder)"
#~ msgstr "AAC (förstörande komprimering, Nero-kodare)"

#~ msgid "Genre"
#~ msgstr "Genre"

#~ msgid "Single Genre"
#~ msgstr "En Genre"

#, fuzzy
#~ msgid ""
#~ "%s was not found in your path. GrimRipper requires it to create %s files.All "
#~ "%s functionality is disabled."
#~ msgstr ""
#~ "%s hittades inte i dina sökvägar. GrimRipper kräver det för att skapa %s-"
#~ "filer. All %s-funktionalitet har inaktiverats."

#, fuzzy
#~ msgid "Playlist file"
#~ msgstr "Spellistfil: "

#, fuzzy
#~ msgid "Music file"
#~ msgstr "Musikfil: "

#~ msgid "Create directory for each album"
#~ msgstr "Skapa katalog för varje album"

#~ msgid "These characters will be removed from all filenames."
#~ msgstr "Dessa tecken kommer att tas bort från alla filnamn."
